// Meiosis State Management
// https://meiosis.js.org

import flyd from 'flyd'
import merge from 'mergerino'
import Actions from './actions.js'
import State from './state.js'


const update$ = flyd.stream()
const states$ = flyd.scan(merge, State(), update$)
const actions = Actions(update$)

const store = {
    states$,
    actions
}

export default store
