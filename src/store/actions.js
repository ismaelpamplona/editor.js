import store from './store.js'

const Actions = (update$) => {
    return {
        changeViewTo(pg) {
            const view = store.states$().view
            Object.keys(view).forEach(key => {
                view[key] = false
            })
            view[pg] = true
            update$({ view })
            console.table(store.states$().view)
        }
    }
}

export default Actions

