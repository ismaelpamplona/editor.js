import '@riotjs/hot-reload'
import { component, install } from 'riot'
import route from 'riot-route'

// importing media
import "./media/favicon.png"
import "./media/app-icon-48x48.png"
import "./media/app-icon-96x96.png"
import "./media/app-icon-144x144.png"
import "./media/app-icon-192x192.png"
import "./media/app-icon-256x256.png"
import "./media/app-icon-384x384.png"
import "./media/app-icon-512x512.png"

import store from './store/store.js'

import './style.scss'

import App from './app.riot'

install(function(component) {
    component.states$ = store.states$
    component.actions = store.actions
    return component
})

component(App)(document.getElementById('root'))

route.base('#/')
route.start(true)

// importing servive worker for pwa - if current browser supports
// if ("serviceWorker" in navigator) {
//     console.log("navigator has service worker");
//     navigator.serviceWorker.register("./sw.js").then(function() {
//         console.log("Service Worker registered");
//     });
// }


