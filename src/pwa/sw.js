// this file sets the SERVICE WORKER for a Progressive Web Application - PWA

console.log("inside the sw.js file");

let CACHE_STATIC_NAME = "static";
let CACHE_DYNAMIC_NAME = "dynamic";
const STATIC_FILES = [
    "/index.html",
    "/bundle.js",
    "/main.css",
    "/fonts/0d7fbc4cc43a12f3fea29c8b55ed1a62.otf",
    "/fonts/6d7cde7d05254fa750d86838319c4ce1.otf",
    "/fonts/00636e0ab9f3199fe0e941df8afced46.otf",
    "/fonts/2923b71fab48e9012c616882d092f00e.otf",
    "/fonts/3647dc96643eb28eef06a0d0ea0892b4.otf",
    "/fonts/71896f83e223ca4a54ef10e2ad79bda8.otf",
    "/fonts/95893071b7f2a535226f5e0c2c1c40c9.otf",
    "/fonts/a70ef3beee075348d7dcdc7743fedd20.otf",
    "/fonts/b8434882ba3d39bc074d34f55586a816.otf",
    "/fonts/dbb7924293b6b1beaa585c775d11b506.otf",
    "/fonts/f2dc48861221dd884f73a353f774c5bd.otf",
    "/fonts/f7339bec7d6baa9a4bf184e63bf013dd.otf",
    "/media/app-icon-48x48.png",
    "/media/app-icon-96x96.png",
    "/media/app-icon-144x144.png",
    "/media/app-icon-192x192.png",
    "/media/app-icon-256x256.png",
    "/media/app-icon-384x384.png",
    "/media/app-icon-512x512.png",
    "/media/favicon.png",
    "/media/loading.gif",
    "/media/logo.png",
];

// https://developer.mozilla.org/en-US/docs/Web/API/Cache
self.addEventListener("install", function(e) {
    console.log("[Service Worker] Installing Service Worker ...", e);
    e.waitUntil(
        caches.open(CACHE_STATIC_NAME).then(function(cache) {
            console.log("[Service Worker] Pre-caching App Shell");
            cache.addAll(STATIC_FILES);
        })
    );
});

self.addEventListener("activate", function(e) {
    console.log("[Service Worker] Activating Service Worker ...", e);
    return self.clients.claim();
});

self.addEventListener("fetch", function(e) {
    console.log("[Service Worker] Fetching Service Worker ...", e);
    e.respondWith(
        caches.match(e.request).then(function(res) {
            if (res) {
                console.log("tem res");
                return res;
            } else {
                console.log("n tem res");
                return fetch(e.request);
            }
        })
    );
});
